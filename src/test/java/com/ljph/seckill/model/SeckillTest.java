package com.ljph.seckill.model;

import com.ljph.seckill.common.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

/**
 * Created by yuzhou on 16/7/31.
 */
public class SeckillTest extends BaseTest {

    private static final Logger _log = LoggerFactory.getLogger(SeckillTest.class);

    @Test
    public void testFindById() throws Exception {
        _log.info("Test find seckill by id");
        Seckill seckill = Seckill.dao.findById(1000L);
        assertNotNull(seckill);
        seckill = Seckill.dao.findById(5000L);
        assertNull(seckill);
    }

    @Before
    public void setUp() throws Exception {
        _log.info("setUp");
        start();
    }

    @After
    public void tearDown() throws Exception {
        _log.info("tearDown");
        stop();
    }
}